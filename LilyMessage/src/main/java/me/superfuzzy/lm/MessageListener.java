/**
 * 
 */
package me.superfuzzy.lm;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import me.superfuzzy.lm.Util.MessageType;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;


/**
 * @author superfuzzy
 *
 */
public class MessageListener {
	
	/**
	 */
	public MessageListener() {
	}
	
	@EventListener
	public void onMessage(MessageEvent me){
		boolean isPlayers = false;
		switch(me.getChannel().toLowerCase(Locale.ENGLISH)){
			case "lmp":
				isPlayers = true;
			case "lmb":
				LilyMessage.debug("sending broadcast...");
				try {
					String messageAll = me.getMessageAsString();
					String[] splits = Util.split(messageAll, "||");
					if(splits.length != (isPlayers ? 3 : 2)){
						throw new IllegalArgumentException("Wrong number of arguments: '||' has occured an unexpected amount: "+splits.length);
					}
					String message = Util.replaceCC(splits[0]);
					MessageType mt = Util.getMessageType(splits[1]);
					Collection<? extends Player> players = Bukkit.getOnlinePlayers();
					if(isPlayers){
//						LilyMessage.debug("handling players");
						String[] playersNames = Util.split(splits[2], ",");
						List<Player> playersArray = new ArrayList<Player>();
						for(String s : playersNames){
//							LilyMessage.debug("searching for Player: "+s+".");
							Player p = Bukkit.getPlayer(s);
							if(p != null){
//								LilyMessage.debug("found Player: "+p.getName());
								playersArray.add(p);
							}
//							LilyMessage.debug("done searching!");
						}
						players = playersArray;
					}
					switch (mt) {
						case ACTIONBAR:
							for(Player p : players){
								TitleManager.sendActionBar(p, message);
							}
							break;
						case NORMAL:
							for(Player p : players){
								p.sendMessage(message);
							}
							break;
						case SUBTITLE:
							for(Player p : players){
								TitleManager.sendTitles(p, ChatColor.RED+"! ALERT !", message, 5, LilyMessage.stay, 5);
							}
							break;
						case TITLE:
							for(Player p : players){
								TitleManager.sendTitles(p, message, " ", 5, LilyMessage.stay, 5);
							}
							break;
						default:
							break;
					}
				} catch (UnsupportedEncodingException e) {
					// should never happen
					e.printStackTrace();
				} catch (IllegalArgumentException e){
					e.printStackTrace();
				}
		}
	}

}
