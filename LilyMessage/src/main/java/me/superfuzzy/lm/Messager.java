/**
 * 
 */
package me.superfuzzy.lm;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;

import javax.naming.NoPermissionException;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.request.RequestException;
import lilypad.client.connect.api.request.impl.MessageRequest;
import me.superfuzzy.lm.Util.MessageType;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

/**
 * @author superfuzzy
 *
 */
public class Messager {

	private Connect	c;
	private MessageListener	ml;

	/**
	 * 
	 */
	public Messager() {
		c = Bukkit.getServicesManager().getRegistration(Connect.class).getProvider();
		ml = new MessageListener();
		c.registerEvents(ml);
	}
	
	public void stop(){
		c.unregisterEvents(ml);
	}

	/**
	 * @param sender 
	 * @param args
	 * @param mt
	 * @param counter
	 * @param emptyList
	 * @throws NoPermissionException 
	 */
	public void sendMessage(CommandSender sender, String[] args, int startindex, List<String> list, boolean isListPlayers) throws NoPermissionException {

		MessageType mt = Util.getMessageType(args[startindex]);
		
		mt.hasSenderPermission(sender);
		
		int counter = startindex;
		if (mt != MessageType.NORMAL) {
			counter++;
		}
		String message = Util.getMessage(args, counter);
		message += "||"+mt.name();
		if(isListPlayers){
			message += "||";
			for(String s : list){
				message += s+",";
			}
			message = message.trim();
			list = Collections.<String>emptyList();
		}
		try {
			c.request(new MessageRequest(list, isListPlayers ? "lmp" : "lmb", message));
			LilyMessage.debug("request sent, message: "+message);
		} catch (UnsupportedEncodingException | RequestException e) {
			sender.sendMessage(Messages.requestFail+e.getMessage());
			e.printStackTrace();
		}
	}

}
