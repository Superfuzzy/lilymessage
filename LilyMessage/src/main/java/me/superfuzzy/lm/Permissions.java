/**
 * 
 */
package me.superfuzzy.lm;

/**
 * @author superfuzzy
 *
 */
public class Permissions {

	public static final String	msgb	= "lilymessage.all";
	public static final String	msgs	= "lilymessage.server";
	public static final String	msgp	= "lilymessage.player";

	public static final String	msg_t	= "lilymessage.special.title";
	public static final String	msg_st	= "lilymessage.special.subtitle";
	public static final String	msg_a	= "lilymessage.special.actionbar";
	
	public static final String	msgr	= "lilymessage.reload";
}
