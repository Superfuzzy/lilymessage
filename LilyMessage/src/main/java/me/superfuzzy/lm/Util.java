package me.superfuzzy.lm;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.naming.NoPermissionException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Util {
	
	
	/**
	 * @author superfuzzy
	 */
	
	public enum MessageType {
		TITLE(Permissions.msg_t), SUBTITLE(Permissions.msg_st), ACTIONBAR(Permissions.msg_a), NORMAL(null);

		private String	permission;

		private MessageType(String permission){
			this.permission = permission;
		}
		
		/**
		 * @param sender
		 * @throws NoPermissionException 
		 */
		public void hasSenderPermission(CommandSender sender) throws NoPermissionException {
			Util.checkPermission(sender, permission);
		}
	}

	public static String[] split(String string, String splitter){
		if(string == null || string == ""){
			return null;
		}
		List<String> strings = new ArrayList<String>();
		String splitsentence = "";
		for(int counter = 0; counter<string.length(); counter++){
			int lenghtToCheck = splitter.length();
			String sToCheck = string.charAt(counter)+"";
			for(int c = 1; c < lenghtToCheck; c++){
				if((counter+c+1)>string.length()){
					break;
				}
				sToCheck += string.charAt(counter+c);
			}
			if(sToCheck.equalsIgnoreCase(splitter)){
				strings.add(splitsentence);
				splitsentence = "";
				counter += sToCheck.length()-1;
			}else{
				splitsentence += string.charAt(counter);
			}
		}
		if(splitsentence != ""){
			strings.add(splitsentence);
		}
		String[] array = new String[strings.size()];
		int c = 0;
		for(String s : strings){
			array[c] = s;
			c++;
		}
		return array;
	}
	
	public static boolean hasSenderPermission(CommandSender sender, String permission) {
		if(sender.hasPermission("*")){
			return true;
		}
		if(sender.hasPermission(permission)){
			return true;
		}
		while(permission.contains(".")){
			String[] permissionsplits = split(permission, ".");
			permission = "";
			for(int counter = 0; counter < (permissionsplits.length-1);counter++){
				if(permission == ""){
					permission = permission+permissionsplits[counter];
				}else{
					permission = permission+"."+permissionsplits[counter];
				}
			}
			if(sender.hasPermission(permission+".*")){
				return true;
			}
		}
		return false;
	}
	
	public static String replaceCC(String text) {
		if(text == null || text == ""){
			return null;
		}
		text = text.replaceAll("&0", ChatColor.BLACK + "");
		text = text.replaceAll("&1", ChatColor.DARK_BLUE + "");
		text = text.replaceAll("&2", ChatColor.DARK_GREEN + "");
		text = text.replaceAll("&3", ChatColor.DARK_AQUA + "");
		text = text.replaceAll("&4", ChatColor.DARK_RED + "");
		text = text.replaceAll("&5", ChatColor.DARK_PURPLE + "");
		text = text.replaceAll("&6", ChatColor.GOLD + "");
		text = text.replaceAll("&7", ChatColor.GRAY + "");
		text = text.replaceAll("&8", ChatColor.DARK_GRAY + "");
		text = text.replaceAll("&9", ChatColor.BLUE + "");
		text = text.replaceAll("&a", ChatColor.GREEN + "");
		text = text.replaceAll("&b", ChatColor.AQUA + "");
		text = text.replaceAll("&c", ChatColor.RED + "");
		text = text.replaceAll("&d", ChatColor.LIGHT_PURPLE + "");
		text = text.replaceAll("&e", ChatColor.YELLOW + "");
		text = text.replaceAll("&f", ChatColor.WHITE + "");
		text = text.replaceAll("&k", ChatColor.MAGIC + "");
		text = text.replaceAll("&l", ChatColor.BOLD + "");
		text = text.replaceAll("&m", ChatColor.STRIKETHROUGH + "");
		text = text.replaceAll("&n", ChatColor.UNDERLINE + "");
		text = text.replaceAll("&o", ChatColor.ITALIC + "");
		text = text.replaceAll("&r", ChatColor.RESET + "");
		return text;
	}

	/**
	 * @param sender
	 * @param msgb
	 * @throws NoPermissionException 
	 */
	public static void checkPermission(CommandSender sender, String permission) throws NoPermissionException {
		if(!hasSenderPermission(sender, permission)){
			sender.sendMessage(Messages.nopermission);
			throw new NoPermissionException("Sender "+sender.getName()+" has not permission: "+permission);
		}
	}

	/**
	 * @param string
	 * @return
	 */
	public static MessageType getMessageType(String string) {
		switch(string.toLowerCase(Locale.ENGLISH)){
			case "title":
				return MessageType.TITLE;
			case "subtitle":
				return MessageType.SUBTITLE;
			case "actionbar":
				return MessageType.ACTIONBAR;
			default:
				return MessageType.NORMAL;
		}
	}

	/**
	 * @param args
	 * @param counter
	 */
	public static String getMessage(String[] args, int startindex) {
		String msg = "";
		int counter = startindex;
		for( ; counter<args.length; counter++){
			msg += " "+args[counter];
		}
		return msg.trim();
	}
}
