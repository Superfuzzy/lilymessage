package me.superfuzzy.lm;


import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.logging.Level;

import javax.naming.NoPermissionException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author superfuzzy
 */

public class LilyMessage extends JavaPlugin{

	private static boolean		debug	= true;
	private static String		pluginname;
	private static LilyMessage	instance;
	public static int	stay = 3*20;
	private Messager	messager;
	private ConfigManager	cm;

	@Override
	public void onLoad() {
		pluginname = getName();
		log("is loading...");
		// TODO
		instance = this;
		log("is now loaded");
	}

	@Override
	public void onEnable() {
		log("is being enabled...");
		messager = new Messager();
		cm = new ConfigManager();
		log("is now enabled");
	}

	@Override
	public void onDisable() {
		log("is being disabled...");
		messager.stop();
		log("is now disabled");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		try {
			//decide if use: getLabel() or getName()
			switch(cmd.getLabel().toLowerCase(Locale.ENGLISH)){
				case "msgb":
					Util.checkPermission(sender, Permissions.msgb);
					if(args.length < 1){
						sender.sendMessage(Messages.wrongargs);
						return false;
					}
					messager.sendMessage(sender, args, 0, Collections.<String> emptyList(), false);
					sender.sendMessage(ChatColor.GREEN+"Broadcast sent!");
					return true;
				case "msgs":
					Util.checkPermission(sender, Permissions.msgs);
					if(args.length < 2){
						sender.sendMessage(Messages.wrongargs);
						return false;
					}
					String[] servers = Util.split(args[0], ",");
					messager.sendMessage(sender, args, 1, Arrays.asList(servers), false);
					sender.sendMessage(ChatColor.GREEN+"Broadcast sent!");
					return true;
				case "msgp":
					Util.checkPermission(sender, Permissions.msgp);
					if(args.length < 2){
						sender.sendMessage(Messages.wrongargs);
						return false;
					}
					String[] players = Util.split(args[0], ",");
					messager.sendMessage(sender, args, 1, Arrays.asList(players), true);
					sender.sendMessage(ChatColor.GREEN+"Broadcast sent!");
					return true;
				case "msgr":
					Util.checkPermission(sender, Permissions.msgr);
					if(args.length < 2){
						sender.sendMessage(Messages.wrongargs);
						return false;
					}
					cm.reload();
					sender.sendMessage(ChatColor.GREEN+"Config reloaded!");
					return true;
				default:
					sender.sendMessage(ChatColor.RED+"Unknown Command!");
					return true;
			}
		} catch (NoPermissionException e) {
			log(e.getMessage());
		}
		return false;
	}

	public static void debug(String message) {
		if (debug) {
			log("[DEBUG] " + message);
		}
	}

	public static void debugSevere(String message) {
		if (debug) {
			logSevere("[DEBUG] " + message);
		}
	}

	public static void debugWarning(String message) {
		if (debug) {
			logWarning("[DEBUG] " + message);
		}
	}

	public static void log(String message) {
		Bukkit.getLogger().log(Level.INFO, "[" + pluginname + "] " + message);
	}

	public static void logSevere(String message) {
		Bukkit.getLogger().log(Level.SEVERE, "[" + pluginname + "] " + message);
	}

	public static void logWarning(String message) {
		Bukkit.getLogger().log(Level.WARNING, "[" + pluginname + "] " + message);
	}

	public static LilyMessage getInstance() {
		if (instance == null) {
			instance = (LilyMessage) Bukkit.getPluginManager().getPlugin(pluginname);
		}
		return instance;
	}
}
