/**
 * 
 */
package me.superfuzzy.lm;

import net.md_5.bungee.api.ChatColor;


/**
 * @author superfuzzy
 *
 */
public class Messages {

	
	
	public static final String	wrongargs	= ChatColor.RED+"[FAIL] "+ChatColor.GRAY+"Wrong number of arguents!";
	public static final String	requestFail	= ChatColor.RED+"[ERROR] "+ChatColor.GRAY+" failed sending Request: ";
	public static final String	nopermission = ChatColor.RED+"[FAIL] "+ChatColor.GRAY+"You don't have permission to do this!";

}
